import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_bloc/constants/enums.dart';
import 'package:learn_bloc/logic/cubit/counter_cubit.dart';
import 'package:learn_bloc/logic/internet_cubit.dart';
import 'package:learn_bloc/presentation/screens/second_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title,this.color}) : super(key: key);

  final String title;
  final Color color;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      backgroundColor: widget.color,

      body: BlocListener<CounterCubit,CounterState>(
        listener: (context,state){
          if(state.wasIncremented==true){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Incremented"),duration: Duration(microseconds: 3000),));
          }
          else if(state.wasIncremented==false){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Decremented"),duration: Duration(microseconds: 3000),));
          }
        },
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              BlocBuilder<InternetCubit,InternetState>(
                builder: (context,state){

                  if(state is InternetConnected && state.connectionsType==ConnectionsType.Wifi){
                    return Text("Wifi");
                  }

                  else if(state is InternetConnected && state.connectionsType==ConnectionsType.Mobile){
                    return Text("Mobile");
                  }
                  else if(state is InternetDisconnected){
                    return Text("Disconnected");
                  }

                  return CircularProgressIndicator();
                },
              ),
              Text(
                'You have pushed the button this many times:',
              ),
              BlocBuilder<CounterCubit,CounterState>(
                builder: (context,state){
                  if(state.counterValue<0){
                    return Text(
                      "BRR, NEGATIVE "+state.counterValue.toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline4,
                    );
                  }
                  else if(state.counterValue %2==0){
                    return Text(
                      "YAAAY "+state.counterValue.toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline4,
                    );
                  }

                  else if(state.counterValue==5){
                    return Text(
                      "HMM,NUMBER "+state.counterValue.toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline4,
                    );
                  }

                  return Text(
                    state.counterValue.toString(),
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline4,
                  );
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  FloatingActionButton(
                    onPressed: (){
                      BlocProvider.of<CounterCubit>(context).decrement();
                    },
                    tooltip: "Decrement",
                    child: Icon(Icons.remove),
                  ),
                  FloatingActionButton(
                    onPressed: (){
                      BlocProvider.of<CounterCubit>(context).increment();
                    },
                    tooltip: "Increment",
                    child: Icon(Icons.add),
                  )
                ],
              ),
              SizedBox(height: 24,),
              MaterialButton(
                onPressed: (){
                  Navigator.of(context).pushNamed("/second");
                },
                child: Text("Go to second Page"),
                color: widget.color,
              ),
              SizedBox(height: 24,),
              MaterialButton(
                onPressed: (){
                  Navigator.of(context).pushNamed("/thired");
                },
                child: Text("Go to Thired Page"),
                color: widget.color,
              )
            ],
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
