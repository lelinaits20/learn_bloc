import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_bloc/logic/cubit/counter_cubit.dart';
import 'package:learn_bloc/presentation/screens/home_screen.dart';
import 'package:learn_bloc/presentation/screens/second_screen.dart';
import 'package:learn_bloc/presentation/screens/thired_screen.dart';

class AppRouter {

  final CounterCubit _counterCubit=CounterCubit();

  Route onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => BlocProvider.value(value:_counterCubit,child: HomeScreen(title: "HomeScreen",color: Colors.blueAccent,)));
        break;
      case '/second':
        return MaterialPageRoute(builder: (_) => BlocProvider.value(value:_counterCubit,child: SecondScreen(title: "SecondScreen",color: Colors.green,)));
        break;
      case '/thired':
        return MaterialPageRoute(builder: (_) => BlocProvider.value(value:_counterCubit,child: ThiredScreen(title: "ThiredScreen",color: Colors.red,)));
        break;

      default:
        return null;
    }
  }

  void dispose(){
    _counterCubit.close();
}
}
