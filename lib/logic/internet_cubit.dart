import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:learn_bloc/constants/enums.dart';
import 'package:meta/meta.dart';

part 'internet_state.dart';

class InternetCubit extends Cubit<InternetState> {

  final Connectivity connectivity;
  StreamSubscription connectivityStreamSubscription;

  InternetCubit({@required this.connectivity}) : super(InternetLoading()){
    connectivityStreamSubscription=connectivity.onConnectivityChanged.listen((connectivityResults) {
      if(connectivityResults==ConnectivityResult.wifi){
        emitInternetConnected(ConnectionsType.Wifi);
      }
      else if(connectivityResults==ConnectivityResult.mobile){
        emitInternetConnected(ConnectionsType.Mobile);
      }
      else if(connectivityResults==ConnectivityResult.none){
        emitInternetDisconnected();
      }
    });
  }

  void emitInternetConnected(ConnectionsType _connectionsType) =>emit(InternetConnected(connectionsType: _connectionsType));

  void emitInternetDisconnected() =>emit(InternetDisconnected());

  @override
  Future<void> close() {
    connectivityStreamSubscription.cancel();
    return super.close();
  }

}
