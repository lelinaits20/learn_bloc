import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_bloc/logic/cubit/counter_cubit.dart';
import 'package:learn_bloc/logic/internet_cubit.dart';
import 'package:learn_bloc/presentation/router/app_router.dart';

void main() {
  runApp(MyApp(appRouter: AppRouter(),connectivity: Connectivity(),));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  final AppRouter appRouter;
  final Connectivity connectivity;


  MyApp({@required this.appRouter, @required this.connectivity});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<InternetCubit>(
          create: (context)=>InternetCubit(connectivity: connectivity),
        ),
        BlocProvider<CounterCubit>(
          create: (context)=>CounterCubit(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: appRouter.onGenerateRoute,
         
      ),
    );
  }
}

